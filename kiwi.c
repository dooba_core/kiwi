/* Dooba SDK
 * Kiwi Bootloader
 */

// External Includes
#include <inttypes.h>
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/boot.h>

// Internal Includes
#include "kiwi.h"

// Current Address
uint32_t caddr;

// Checksum
uint16_t csum;

// Counters, etc...
uint8_t c;
uint16_t i;

// Data Buffer
uint8_t kiwi_buf[KIWI_PGSIZE];

// Watchdog Reset
void kiwi_watchdog_reset()
{
	__asm__ __volatile__("wdr\n");
}

// Configure Watchdog
void kiwi_watchdog_config(uint8_t x)
{
	WDTCSR = _BV(WDCE) | _BV(WDE);
	WDTCSR = x;
}

// Start Application
void kiwi_start_app(uint8_t rst_flags)
{
	// Save reset flags & Jump into Application
	asm volatile("mov r2, %0\n" :: "r" (rst_flags));
	kiwi_wd_set(KIWI_WD_OFF);
	asm volatile(
	"ldi r30,%[rstvec]\n"
	"clr r31\n"
	"ijmp\n"::[rstvec] "M"(kiwi_app)
	);
}

// Commit Page to Program Memory
void kiwi_commit(uint16_t addr)
{
	// Copy Buffer Data
	uint16_t len = KIWI_PGSIZE;
	uint8_t *buf_p = kiwi_buf;
	uint16_t addr_p = (uint16_t)(void *)addr;

	// Erase Page
	kiwi_erase_page((uint16_t)(void *)addr);
	kiwi_spm_wait();

	// Copy to Flash Write Buffer
	do
	{
		uint16_t a;
		a = *buf_p++;
		a |= (*buf_p++) << 8;
		kiwi_fill_page((uint16_t)(void *)addr_p, a);
		addr_p += 2;
	} while(len -= 2);

	// Write Flash
	kiwi_write_page((uint16_t)(void *)addr);
	kiwi_spm_wait();

	// Re-Enable Read Access
	kiwi_enable_rww();
}

// Send
void send(uint8_t v)
{
	while(!(UCSR0A & _BV(UDRE0)))											{ /* NoOp */ }
	UDR0 = v;
}

// Recv
uint8_t recv()
{
	uint8_t v;
	while(!(UCSR0A & _BV(RXC0)))											{ /* NoOp */ }
	v = UDR0;
	return v;
}

// Main
int main()
{
	// Clear Interrupts
	cli();

	// Check for WDT Reset { Start Application }
	c = MCUSR;
	MCUSR = 0;
	if(c & _BV(WDRF))														{ kiwi_start_app(c); }

	// Init LED Pin & Flash
	KIWI_LED_DDR |= _BV(KIWI_LED_P);
	kiwi_led_on();

	// Configure UART
	PRR0 &= ~(1 << PRUSART0);
	UCSR0B = _BV(RXEN0) | _BV(TXEN0);
	UCSR0C = (3 << UCSZ00);
	UBRR0H = (KIWI_PRESCALE >> 8);
	UBRR0L = KIWI_PRESCALE & 0xff;

	// Start application after a few seconds if nothing happens
	kiwi_wd_set(KIWI_WD_ON);

	// Loop until Watchdog Reset
	caddr = 0;
	for(;;)
	{
		// Read Command, Handle Command & Send Response
		csum = 0;
		c = recv();
		kiwi_wd_set(KIWI_WD_OFF);
		if(c == KIWI_CMD_SET_ADDR)											{ caddr = 0; for(i = 0; i < 3; i = i + 1) { caddr = (caddr << 8) | recv(); } for(i = 0; i < 3; i = i + 1) { send(caddr >> (8 * (2 - i))); } RAMPZ = (caddr & 0x10000) ? 1 : 0; }
		else if(c == KIWI_CMD_GET_INFO)										{ send(boot_lock_fuse_bits_get(GET_LOW_FUSE_BITS)); send(boot_lock_fuse_bits_get(GET_HIGH_FUSE_BITS)); send(boot_lock_fuse_bits_get(GET_EXTENDED_FUSE_BITS)); send(boot_lock_fuse_bits_get(GET_LOCK_BITS)); }
		else if(c == KIWI_CMD_SET_LFUSE)									{ recv(); send(boot_lock_fuse_bits_get(GET_LOW_FUSE_BITS)); }
		else if(c == KIWI_CMD_SET_HFUSE)									{ recv(); send(boot_lock_fuse_bits_get(GET_HIGH_FUSE_BITS)); }
		else if(c == KIWI_CMD_SET_EFUSE)									{ recv(); send(boot_lock_fuse_bits_get(GET_EXTENDED_FUSE_BITS)); }
		else if(c == KIWI_CMD_SET_LOCKB)									{ recv(); send(boot_lock_fuse_bits_get(GET_LOCK_BITS)); }
		else if(c == KIWI_CMD_PROG_PAGE)									{ for(i = 0; i < KIWI_PGSIZE; i = i + 1) { kiwi_buf[i] = recv(); csum = csum + kiwi_buf[i]; } send((csum >> 8) & 0x00ff); send(csum & 0x00ff); kiwi_commit(caddr & 0x0000ffff); }
		else if(c == KIWI_CMD_DONE)											{ kiwi_wd_set(KIWI_WD_NOW); }
		else																{ /* NoOp */ }
		send(KIWI_RES_OK);
	}
}
